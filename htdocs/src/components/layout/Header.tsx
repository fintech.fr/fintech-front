import React from 'react';
import { Menu } from 'semantic-ui-react';
import { Link, useHistory } from 'react-router-dom';
import { useAuthService } from '../../ioc/hooks';
import { useBehaviorSubject } from '../../hooks/behavior-subject.hook';

export const Header = () => {

    const authService = useAuthService();
    const history = useHistory();

    const isAuthenticated = useBehaviorSubject(authService.isAuthenticated$);
    const username = useBehaviorSubject(authService.username$);

    const onLogoutClickHandler = () => {
        authService.logout();
        history.push('/');
    }

    return (
        <Menu>
            <Menu.Item><img src='https://via.placeholder.com/150' /></Menu.Item>
            <Menu.Item name='home' as={Link} to='/home'>Home</Menu.Item>
            {isAuthenticated && <>
                <Menu.Item name='create-project' as={Link} to='/create-project'>Créer un nouveau projet</Menu.Item>
            </>}
            <Menu.Menu position='right'>
                {!isAuthenticated && <>
                    <Menu.Item name='sign-up' as={Link} to='/sign-in'>Se connecter</Menu.Item>
                    <Menu.Item name='sign-in' as={Link} to='/sign-up'>S'inscrire</Menu.Item>
                </>}
                {isAuthenticated && <>
                    <Menu.Item name='profile'>{username}</Menu.Item>
                    <Menu.Item name='sign-out' onClick={onLogoutClickHandler}>Se déconnecter</Menu.Item>
                </>}
            </Menu.Menu>
        </Menu>
    );
};