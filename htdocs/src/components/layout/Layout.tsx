import React from 'react';
import { Header } from './Header';
import { Routes } from '../../router/routes';

export const Layout = () => {
    return (
        <div>
            <Header />
            <main>
                <Routes />
            </main>
        </div>
    );
}