import React, { useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Form, Button } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom';
import { useAuthService } from '../../ioc/hooks';
import { SignInForm } from '../../models/sign-in-form';

export const SignIn = () => {

    const { handleSubmit, errors, control, setError } = useForm<SignInForm>();

    const authService = useAuthService();
    const history = useHistory();

    const onSubmit = handleSubmit((form: SignInForm) => {
        authService.postAuth(form).subscribe(result => {
            const { token, refresh_token } = result.data;
            authService.setAuthToken(token, refresh_token);
            history.push('/');
        }, error => {
            if (error.response.status === 401) {
                setError('password', 'noMatch', error.response.data.message);
            } else {
                // TODO
            }
        })
    });

    return (
        <Form onSubmit={onSubmit}>
            <Controller
                as={
                    <Form.Input
                    required
                    error={errors?.email?.message}
                    fluid
                    label="Email"
                    placeholder="Email"
                    />
                }
                control={control}
                rules={{ required: true }}
                name="email"
                defaultValue=""
            />
            <Controller
                as={
                    <Form.Input
                    required
                    error={errors?.password?.message}
                    fluid
                    type="password"
                    label="Password"
                    placeholder="Password"
                    />
                }
                control={control}
                rules={{ required: true }}
                name="password"
                defaultValue=""
            />
            <Button type="submit">Sign in</Button>
        </Form>
    );

}