import React from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Form, Button } from 'semantic-ui-react';
import { useHistory } from 'react-router-dom'
import { useAuthService } from '../../ioc/hooks';
import { SignUpForm } from '../../models/sign-up-form';

export const SignUp = () => {

    const { handleSubmit, errors, control, watch, setError } = useForm<SignUpForm>();

    const authService = useAuthService();
    const history = useHistory();

    const onSubmit = (form: SignUpForm) => {
        authService.postSignUp(form).subscribe(result => {
            console.log(result);
            history.push('/');
        }, error => {
            if (error.response.status === 400) {
                // J'assume que si il y a une 400 alors c'est que l'email est déjà pris : pas bien
                setError('email', 'alreadyExists', error.response.data.detail);
            } else {
                // TODO
            }
        });
    }

    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <Controller
                as={
                    <Form.Input
                        required
                        error={errors?.firstName?.message}
                        fluid
                        label="FirstName"
                        placeholder="FirstName"
                    />
                }
                control={control}
                rules={{ required: "Required"}}
                name="firstName"
                defaultValue=""
            />
            <Controller
                as={
                    <Form.Input
                        required
                        error={errors?.lastName?.message}
                        fluid
                        label="LastName"
                        placeholder="LastName"
                    />
                }
                control={control}
                rules={{ required: "Required"}}
                name="lastName"
                defaultValue=""
            />
            <Controller
                as={
                    <Form.Input
                        required
                        error={errors?.email?.message}
                        fluid
                        type="email"
                        label="Email"
                        placeholder="Email"
                    />
                }
                control={control}
                rules={{
                    required: "Required",
                    pattern: {
                        message: "Not an email",
                        value: /^\S+@\S+$/
                    }
                }}
                name="email"
                defaultValue=""
            />
            <Controller
                as={
                    <Form.Input
                        required
                        error={errors?.password?.message}
                        fluid
                        type="password"
                        label="Password"
                        placeholder="Password"
                    />
                }
                control={control}
                rules={{required: "Required"}}
                name="password"
                defaultValue=""
            />
            <Controller
                as={
                    <Form.Input
                        required
                        error={errors?.confirmPassword?.message}
                        fluid
                        type="password"
                        label="Confirm password"
                        placeholder="Confirm password"
                    />
                }
                control={control}
                rules={{
                    required: "Required",
                    validate: confirmPassword => confirmPassword === watch('password') || "Passwords don't match"
                }}
                name="confirmPassword"
                defaultValue=""
            />
            <Controller
                as={
                    <Form.Input
                        required
                        error={errors?.birthday?.message}
                        fluid
                        type="date"
                        label="Birthday"
                        placeholder="Birthday"
                    />
                }
                control={control}
                rules={{
                    required: "Required"
                }}
                name="birthday"
                defaultValue=""
            />
            <Button type="submit">S'inscrire</Button>
        </Form>
    );
}