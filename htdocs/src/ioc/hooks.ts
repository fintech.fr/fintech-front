import { useMemo } from 'react';
import { interfaces } from 'inversify';
import { container } from './config';
import { AuthService } from '../services/auth-service';
import { TYPES } from './types';

export function useService<T>(type: interfaces.ServiceIdentifier<T>): T {
    const service = useMemo(() => container.get<T>(type), [type]);
    return service;
}

// Shortcut hooks
export const useAuthService = () => useService<AuthService>(TYPES.AuthService);