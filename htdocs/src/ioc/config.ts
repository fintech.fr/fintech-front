import { Container } from "inversify";
import { TYPES } from "./types";
import { AuthService } from "../services/auth-service";
import { IConfig } from "../config/iconfig";
import { Config } from '../config';

const container = new Container();

container.bind(TYPES.AuthService).to(AuthService).inSingletonScope();
container.bind<IConfig>(TYPES.Config).toConstantValue(Config);

export { container };