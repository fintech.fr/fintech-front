export type AuthenticationTokenResponse = {
    token: string;
    refresh_token: string;
}
