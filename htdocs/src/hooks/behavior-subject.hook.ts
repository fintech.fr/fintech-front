import { BehaviorSubject } from 'rxjs';
import { useState, useEffect } from 'react';

export function useBehaviorSubject<T>(bs: BehaviorSubject<T>): T {
    const [val, setVal] = useState(bs.value);

    useEffect(() => {
        const subscription = bs.subscribe(v => setVal(v));

        return () => {
            subscription.unsubscribe();
        };
    }, []);

    return val;
}