import { Config as dev } from './config.dev';
import { Config as prod } from './config.prod';
import { IConfig } from './iconfig';

export const Config: IConfig = process.env.NODE_ENV === 'development' ? dev : prod;
