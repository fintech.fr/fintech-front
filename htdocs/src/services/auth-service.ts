import { injectable, inject, postConstruct } from "inversify";
import { BehaviorSubject } from "rxjs";
import { decode } from 'jsonwebtoken';
import Axios from 'axios-observable';
import { AuthenticationTokenResponse } from "../models/authentication-token-response";
import { IConfig } from "../config/iconfig";
import { TYPES } from "../ioc";
import { SignUpForm } from "../models/sign-up-form";
import { SignInForm } from "../models/sign-in-form";

const AUTH_TOKEN_KEY = 'APP_AUTH_TOKEN';
const AUTH_REFRESH_TOKEN_KEY = 'APP_AUTH_REFRESH_TOKEN';

type JwtData = {
    username: string;
    exp: number;
    roles: [string];
}

@injectable()
export class AuthService {

    isAuthenticated$ = new BehaviorSubject(false);
    roles$ = new BehaviorSubject<Nullable<[string]>>(null);
    username$ = new BehaviorSubject<Nullable<string>>(null);

    private refreshTimeoutId: Nullable<NodeJS.Timeout> = null;

    @inject(TYPES.Config) config!: IConfig;

    @postConstruct()
    init() {
        const token = localStorage.getItem(AUTH_TOKEN_KEY);
        const refreshToken = localStorage.getItem(AUTH_REFRESH_TOKEN_KEY);

        if (token && refreshToken) {
            this.setAuthToken(token, refreshToken);
        }
    }

    postAuth(form: SignInForm) {
        return Axios.post<AuthenticationTokenResponse>(`${this.config.baseApiUrl}/authentication_token`, form);
    }

    postSignUp(form: SignUpForm) {
        return Axios.post<AuthenticationTokenResponse>(`${this.config.baseApiUrl}/api/users`, form);
    }

    setAuthToken(token: string, refreshToken: string): void {
        const { username, exp, roles } = decode(token) as JwtData;

        const timeBeforeExp = (exp * 1000) - new Date().getTime();

        if (timeBeforeExp < 0) { // token already expired
            return;
        }

        localStorage.setItem(AUTH_TOKEN_KEY, token);
        localStorage.setItem(AUTH_REFRESH_TOKEN_KEY, refreshToken);

        this.isAuthenticated$.next(true);
        this.roles$.next(roles);
        this.username$.next(username);

        Axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

        if (this.refreshTimeoutId !== null) {
            clearTimeout(this.refreshTimeoutId); // cancel refresh timeout
        }

        this.refreshTimeoutId = setTimeout(() => {
            this.refreshTimeoutId = null;
            Axios.post(`${this.config.baseApiUrl}/refresh_token`, { refresh_token: refreshToken })
                .subscribe(result => {
                    const { token, refresh_token } = result.data;
                    this.setAuthToken(token, refresh_token);
                }, error => {
                    console.error(error);
                    this.logout();
                });
        }, timeBeforeExp);
    }

    logout(): void {
        this.isAuthenticated$.next(false);
        this.roles$.next(null);
        this.username$.next(null);

        localStorage.removeItem(AUTH_TOKEN_KEY);
        localStorage.removeItem(AUTH_REFRESH_TOKEN_KEY);

        delete Axios.defaults.headers.common['Authorization'];

        if (this.refreshTimeoutId !== null) {
            clearTimeout(this.refreshTimeoutId);
            this.refreshTimeoutId = null;
        }
    }
}