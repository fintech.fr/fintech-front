import React from 'react';
import { Layout } from './components/layout/Layout';
import { BrowserRouter } from 'react-router-dom'

import 'semantic-ui-css/semantic.min.css';

const App = () => (
  <BrowserRouter>
    <Layout />
  </BrowserRouter>
);

export default App;
