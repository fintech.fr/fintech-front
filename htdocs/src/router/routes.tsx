import React from 'react';
import { Switch, Route, Redirect, RouteProps } from 'react-router-dom';
import { Home } from '../components/pages/home';
import { SignIn } from '../components/pages/sign-in';
import { SignUp } from '../components/pages/sign-up';
import { Page404 } from '../components/pages/page404';
import { useAuthService } from '../ioc/hooks';
import { useBehaviorSubject } from '../hooks/behavior-subject.hook';
import { CreateProject } from '../components/pages/create-project';
import { Page403 } from '../components/pages/page403';

export const Routes = () => {

    const authService = useAuthService();
    const isAuthenticated = useBehaviorSubject(authService.isAuthenticated$);

    // Route nécessitant d'être authentifié
    const AuthRoute = (props: RouteProps) => {
        if (isAuthenticated) {
            return <Route {...props} />
        } else {
            return <Route {...{props, component: Page403}} />
        }
    }

    return (
        <Switch>
            <Route exact path='/home' component={Home} />
            <Route exact path='/sign-in' component={SignIn} />
            <Route exact path='/sign-up' component={SignUp} />
            <Redirect from='/' exact to='/home' />

            <AuthRoute exact path='/create-project' component={CreateProject} />

            <Route component={Page404} />
        </Switch>
    );
}
